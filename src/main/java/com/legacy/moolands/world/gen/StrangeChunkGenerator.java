package com.legacy.moolands.world.gen;

import java.util.Random;
import java.util.function.Supplier;

import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.world.gen.noise.BetaOctavesNoiseGenerator;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.Blockreader;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeContainer;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.DimensionSettings;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.spawner.WorldEntitySpawner;

public class StrangeChunkGenerator extends ChunkGenerator
{
	public static final Codec<StrangeChunkGenerator> providerCodec = RecordCodecBuilder.create((p_236091_0_) ->
	{
		return p_236091_0_.group(BiomeProvider.CODEC.fieldOf("biome_source").forGetter((p_236096_0_) ->
		{
			return p_236096_0_.biomeProvider;
		}), Codec.LONG.fieldOf("seed").stable().forGetter((p_236093_0_) ->
		{
			return p_236093_0_.seed;
		}), DimensionSettings.field_236098_b_.fieldOf("settings").forGetter((p_236090_0_) ->
		{
			return p_236090_0_.settings;
		})).apply(p_236091_0_, p_236091_0_.stable(StrangeChunkGenerator::new));
	});

	private Random rand;
	private BetaOctavesNoiseGenerator noise_generator;
	private double[] noise;
	private double[] noiseArray;
	private final Supplier<DimensionSettings> settings;
	public static final int CHUNK_SIZE = 16;
	public long seed;

	public StrangeChunkGenerator(BiomeProvider biomeProvider, long seed, Supplier<DimensionSettings> settings)
	{
		super(biomeProvider, biomeProvider, settings.get().getStructures(), seed);

		rand = new Random(seed);
		this.noise_generator = new BetaOctavesNoiseGenerator(this.rand, 4);
		this.settings = settings;
	}

	protected Codec<? extends ChunkGenerator> func_230347_a_()
	{
		return providerCodec;
	}

	// generateSurface
	@Override
	public void generateSurface(WorldGenRegion genregion, IChunk chunk)
	{
		this.replaceBlocksForBiome(chunk.getPos().x, chunk.getPos().z, chunk, chunk.getBiomes());
	}

	public void spawnMobs(WorldGenRegion region)
	{
		int i = region.getMainChunkX();
		int j = region.getMainChunkZ();
		Biome biome = region.getBiome((new ChunkPos(i, j)).asBlockPos());
		SharedSeedRandom sharedseedrandom = new SharedSeedRandom();
		sharedseedrandom.setDecorationSeed(region.getSeed(), i << 4, j << 4);
		WorldEntitySpawner.performWorldGenSpawning(region, biome, i, j, sharedseedrandom);
	}

	// makeBase
	@Override
	public void func_230352_b_(IWorld worldIn, StructureManager structureManagerIn, IChunk chunkIn)
	{
		int x = chunkIn.getPos().x;
		int z = chunkIn.getPos().z;
		rand.setSeed((long) x * 341873128712L + (long) z * 132897987541L);
		// biomesForGeneration = biomeProvider.getBiomes(x * 16, z * 16, 16, 16, false);
		this.setBlocksInChunk(chunkIn);
		// chunkIn.setBiomes(biomesForGeneration);
	}

	private void setBlocksInChunk(IChunk chunk)
	{
		BlockPos pos;
		this.noiseArray = generateNoise(this.noiseArray, chunk.getPos().x * 2, chunk.getPos().z * 2);

		for (int i1 = 0; i1 < 2; i1++)
		{
			for (int j1 = 0; j1 < 2; j1++)
			{
				for (int k1 = 0; k1 < 32; k1++)
				{
					double d1 = this.noiseArray[(i1 * 34 + j1) * 33 + k1];
					double d2 = this.noiseArray[(i1 * 34 + (j1 + 1)) * 33 + k1];
					double d3 = this.noiseArray[((i1 + 1) * 34 + j1) * 33 + k1];
					double d4 = this.noiseArray[((i1 + 1) * 34 + (j1 + 1)) * 33 + k1];
					double d5 = (this.noiseArray[(i1 * 34 + j1) * 33 + (k1 + 1)] - d1) * 0.25D;
					double d6 = (this.noiseArray[(i1 * 34 + (j1 + 1)) * 33 + (k1 + 1)] - d2) * 0.25D;
					double d7 = (this.noiseArray[((i1 + 1) * 34 + j1) * 33 + (k1 + 1)] - d3) * 0.25D;
					double d8 = (this.noiseArray[((i1 + 1) * 34 + (j1 + 1)) * 33 + (k1 + 1)] - d4) * 0.25D;

					for (int l1 = 0; l1 < 4; l1++)
					{
						double d10 = d1;
						double d11 = d2;
						double d12 = (d3 - d1) * 0.125D;
						double d13 = (d4 - d2) * 0.125D;

						for (int i2 = 0; i2 < 8; i2++)
						{
							double d15 = d10;
							double d16 = (d11 - d10) * 0.125D;

							for (int k2 = 0; k2 < 8; k2++)
							{
								int x = i2 + i1 * 8;
								int z = k2 + j1 * 8;
								int y = k1 * 4 + l1;

								BlockState filler = Blocks.AIR.getDefaultState();

								if (d15 > 0.0D)
								{
									filler = MoolandsBlocks.moo_rock.getDefaultState();
								}

								pos = new BlockPos(x, y, z);

								// WE ALL NEED AN AGILTIY CAPE IN OUR LIFE
								if (y >= 0 && y <= 127)
								{
									chunk.setBlockState(pos, filler, false);
								}

								d15 += d16;
							}

							d10 += d12;
							d11 += d13;
						}

						d1 += d5;
						d2 += d6;
						d3 += d7;
						d4 += d8;
					}

				}

			}

		}
	}

	private double[] generateNoise(double ad[], int i, int k)
	{
		if (ad == null)
		{
			ad = new double[3366];
		}

		this.noise = this.noise_generator.generateNoiseOctaves(this.noise, i, 0, k, 3, 33, 34, 1D, 1D, 1D);

		int id = 0;

		for (int l1 = 0; l1 < 3; l1++)
		{
			for (int i2 = 0; i2 < 34; i2++)
			{
				for (int j2 = 0; j2 < 33; j2++)
				{
					ad[id] = this.noise[id] + ((double) (j2 / 33) - 0.5D);
					id++;
				}

			}
		}

		return ad;
	}

	private void replaceBlocksForBiome(int chunkX, int chunkZ, IChunk chunk, BiomeContainer biomeContainer)
	{
		for (int k = 0; k < 16; k++)
		{
			for (int l = 0; l < 16; l++)
			{
				int i1 = (int) (3.0D + this.rand.nextDouble() * 0.25D);

				int j1 = -1;

				BlockState top = MoolandsBlocks.mooland_grass_block.getDefaultState();
				BlockState filler = MoolandsBlocks.mooland_dirt.getDefaultState();

				for (int k1 = 127; k1 >= 0; k1--)
				{
					Block block = chunk.getBlockState(new BlockPos(k, k1, l)).getBlock();

					if (block == Blocks.AIR)
					{
						j1 = -1;
					}
					else if (block == MoolandsBlocks.moo_rock)
					{
						if (j1 == -1)
						{
							if (i1 <= 0)
							{
								top = Blocks.AIR.getDefaultState();
								filler = MoolandsBlocks.moo_rock.getDefaultState();
							}

							j1 = i1;

							if (k1 >= 0)
							{
								chunk.setBlockState(new BlockPos(k, k1, l), top, false);
							}
							else
							{
								chunk.setBlockState(new BlockPos(k, k1, l), filler, false);
							}

						}
						else if (j1 > 0)
						{
							--j1;
							chunk.setBlockState(new BlockPos(k, k1, l), filler, false);
						}
					}
				}
			}
		}
	}

	@Override
	public ChunkGenerator func_230349_a_(long seedIn)
	{
		return new StrangeChunkGenerator(this.biomeProvider.getBiomeProvider(seedIn), seedIn, this.settings);
	}

	@Override
	public int getHeight(int x, int z, Type heightmapType)
	{
		return 0;
	}

	@Override
	public IBlockReader func_230348_a_(int p_230348_1_, int p_230348_2_)
	{
		BlockState[] ablockstate = new BlockState[1];
		//this.func_236087_a_(p_230348_1_, p_230348_2_, ablockstate, (Predicate<BlockState>) null);
		return new Blockreader(ablockstate);
	}
}