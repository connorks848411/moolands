package com.legacy.moolands.block;

import java.util.Random;
import java.util.function.Supplier;

import com.legacy.structure_gel.blocks.GelPortalBlock;
import com.legacy.structure_gel.util.GelTeleporter;
import com.legacy.structure_gel.util.GelTeleporter.CreatePortalBehavior;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.village.PointOfInterestType;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class MoolandPortalBlock extends GelPortalBlock
{
	public MoolandPortalBlock(Supplier<RegistryKey<World>> startingDimensionIn, Supplier<RegistryKey<World>> endpointDimensionIn, Supplier<PointOfInterestType> portalPOI, Supplier<GelPortalBlock> portalBlockIn, Supplier<BlockState> frameBlockIn)
	{
		super(Block.Properties.from(Blocks.NETHER_PORTAL), (world) -> new GelTeleporter(world, startingDimensionIn, endpointDimensionIn, portalPOI, portalBlockIn, frameBlockIn, CreatePortalBehavior.NETHER));
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		if (rand.nextInt(100) == 0)
		{
			worldIn.playSound((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, SoundEvents.BLOCK_PORTAL_AMBIENT, SoundCategory.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);
		}

		for (int i = 0; i < 2; ++i)
		{
			double d0 = (double) ((float) pos.getX() + rand.nextFloat());
			double d1 = (double) ((float) pos.getY() + rand.nextFloat());
			double d2 = (double) ((float) pos.getZ() + rand.nextFloat());
			int j = rand.nextInt(2) * 2 - 1;

			if (worldIn.getBlockState(pos.west()).getBlock() != this && worldIn.getBlockState(pos.east()).getBlock() != this)
			{
				d0 = (double) pos.getX() + 0.5D + 0.25D * (double) j;
			}
			else
			{
				d2 = (double) pos.getZ() + 0.5D + 0.25D * (double) j;
			}

			worldIn.addParticle(ParticleTypes.CLOUD, d0, d1, d2, 0, 0, 0);
		}
	}
}