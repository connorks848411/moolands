package com.legacy.moolands.block;

import java.util.List;
import java.util.Random;

import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GrassBlock;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.material.Material;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.FlowersFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.PlantType;

public class MoolandGrassBlock extends GrassBlock
{
	public MoolandGrassBlock(Block.Properties props)
	{
		super(props);
	}

	@Override
	public BlockState updatePostPlacement(BlockState state, Direction facing, BlockState facingState, IWorld worldIn, BlockPos pos, BlockPos facingPos)
	{
		Block block = worldIn.getBlockState(pos.up()).getBlock();
		return state.with(SNOWY, Boolean.valueOf(block == Blocks.SNOW || block == Blocks.SNOW));
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
	{
		Block dirtBlock = null;
		Block grassBlock = null;

		if (this == MoolandsBlocks.mooland_grass_block)
		{
			dirtBlock = MoolandsBlocks.mooland_dirt;
			grassBlock = MoolandsBlocks.mooland_grass_block;
		}

		if (!worldIn.isRemote)
		{
			if (worldIn.getLight(pos.up()) < 4 && worldIn.getBlockState(pos.up()).getOpacity(worldIn, pos.up()) > 2 || worldIn.getBlockState(pos.up()).getBlock() == Blocks.WATER)
			{
				worldIn.setBlockState(pos, dirtBlock.getDefaultState());
			}
			else
			{
				if (worldIn.getLight(pos.up()) >= 9)
				{
					for (int i = 0; i < 4; ++i)
					{
						BlockPos blockpos = pos.add(rand.nextInt(3) - 1, rand.nextInt(5) - 3, rand.nextInt(3) - 1);

						if (blockpos.getY() >= 0 && blockpos.getY() < 256 && !worldIn.isBlockLoaded(blockpos))
						{
							return;
						}

						BlockState iblockstate = worldIn.getBlockState(blockpos.up());
						BlockState iblockstate1 = worldIn.getBlockState(blockpos);

						if (iblockstate1.getBlock() == dirtBlock && worldIn.getLight(blockpos.up()) >= 4 && iblockstate.getOpacity(worldIn, pos.up()) <= 2 && iblockstate.getBlock() != Blocks.WATER)
						{
							worldIn.setBlockState(blockpos, grassBlock.getDefaultState());
						}
					}
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	@Override
	public void grow(ServerWorld worldIn, Random rand, BlockPos pos, BlockState state)
	{
		BlockPos blockpos = pos.up();
		BlockState blockstate = MoolandsBlocks.mooland_grass.getDefaultState();

		label48: for (int i = 0; i < 128; ++i)
		{
			BlockPos blockpos1 = blockpos;

			for (int j = 0; j < i / 16; ++j)
			{
				blockpos1 = blockpos1.add(rand.nextInt(3) - 1, (rand.nextInt(3) - 1) * rand.nextInt(3) / 2, rand.nextInt(3) - 1);
				if (!worldIn.getBlockState(blockpos1.down()).isIn(this) || worldIn.getBlockState(blockpos1).hasOpaqueCollisionShape(worldIn, blockpos1))
				{
					continue label48;
				}
			}

			BlockState blockstate2 = worldIn.getBlockState(blockpos1);
			if (blockstate2.isIn(blockstate.getBlock()) && rand.nextInt(10) == 0)
			{
				((IGrowable) blockstate.getBlock()).grow(worldIn, rand, blockpos1, blockstate2);
			}

			if (blockstate2.isAir())
			{
				BlockState blockstate1;
				if (rand.nextInt(8) == 0)
				{
					List<ConfiguredFeature<?, ?>> list = worldIn.getBiome(blockpos1).getGenerationSettings().getFlowerFeatures();
					if (list.isEmpty())
					{
						continue;
					}

					ConfiguredFeature<?, ?> configuredfeature = list.get(0);
					FlowersFeature flowersfeature = (FlowersFeature) configuredfeature.feature;
					blockstate1 = flowersfeature.getFlowerToPlace(rand, blockpos1, configuredfeature.func_242767_c());
				}
				else
				{
					blockstate1 = blockstate;
				}

				if (blockstate1.isValidPosition(worldIn, blockpos1))
				{
					worldIn.setBlockState(blockpos1, blockstate1, 3);
				}
			}
		}

	}

	@Override
	public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, net.minecraftforge.common.IPlantable plantable)
	{
		if (plantable instanceof SaplingBlock)
			return true;

		PlantType plantType = plantable.getPlantType(world, pos.offset(facing));

		if (plantType.equals(PlantType.PLAINS))
			return true;
		else if (plantType.equals(PlantType.BEACH))
			return (world.getBlockState(pos.east()).getMaterial() == Material.WATER || world.getBlockState(pos.west()).getMaterial() == Material.WATER || world.getBlockState(pos.north()).getMaterial() == Material.WATER || world.getBlockState(pos.south()).getMaterial() == Material.WATER);
		else
			return super.canSustainPlant(state, world, pos, facing, plantable);
	}

	@Override
	public void onPlantGrow(BlockState state, IWorld world, BlockPos pos, BlockPos source)
	{
		world.setBlockState(pos, MoolandsBlocks.mooland_dirt.getDefaultState(), 2);
	}
}