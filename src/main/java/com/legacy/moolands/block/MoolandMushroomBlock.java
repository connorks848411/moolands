package com.legacy.moolands.block;

import java.util.Random;

import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.registry.MoolandsFeatures;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.IGrowable;
import net.minecraft.block.MushroomBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.BigMushroomFeatureConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.server.ServerWorld;

public class MoolandMushroomBlock extends MushroomBlock implements IGrowable
{
	public MoolandMushroomBlock(Block.Properties properties)
	{
		super(properties);
	}

	@Override
	public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos)
	{
		BlockPos blockpos = pos.down();
		BlockState blockstate = worldIn.getBlockState(blockpos);
		Block block = blockstate.getBlock();

		if (block != MoolandsBlocks.mooland_grass_block && block != MoolandsBlocks.mooland_dirt)
		{
			return worldIn.getLightSubtracted(pos, 0) < 13 && blockstate.canSustainPlant(worldIn, blockpos, net.minecraft.util.Direction.UP, this);
		}
		else
		{
			return true;
		}
	}

	@Override
	public boolean canGrow(IBlockReader worldIn, BlockPos pos, BlockState state, boolean isClient)
	{
		return this == MoolandsBlocks.blue_mushroom;
	}

	@Override
	public boolean grow(ServerWorld world, BlockPos pos, BlockState state, Random random)
	{
		world.removeBlock(pos, false);
		ConfiguredFeature<BigMushroomFeatureConfig, ?> configuredfeature;

		if (this == MoolandsBlocks.blue_mushroom)
		{
			configuredfeature = MoolandsFeatures.BLUE_MUSHROOM.withConfiguration(new BigMushroomFeatureConfig(new SimpleBlockStateProvider(MoolandsBlocks.blue_mushroom_block.getDefaultState()), new SimpleBlockStateProvider(Blocks.MUSHROOM_STEM.getDefaultState()), 2));
		}
		else
		{
			world.setBlockState(pos, state, 3);
			return false;
		}

		if (configuredfeature.func_242765_a(world, world.getChunkProvider().getChunkGenerator(), random, pos))
		{
			return true;
		}
		else
		{
			world.setBlockState(pos, state, 3);
			return false;
		}
	}
}