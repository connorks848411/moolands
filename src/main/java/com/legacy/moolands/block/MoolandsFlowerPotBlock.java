package com.legacy.moolands.block;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FlowerPotBlock;
import net.minecraft.block.material.Material;

public class MoolandsFlowerPotBlock extends FlowerPotBlock
{
	public MoolandsFlowerPotBlock(@Nullable Supplier<FlowerPotBlock> emptyPot, Supplier<? extends Block> flower, Block.Properties properties)
	{
		super(emptyPot, flower, properties);
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(flower.get().getRegistryName(), () -> this);
	}

	public MoolandsFlowerPotBlock(Supplier<? extends Block> flower, Block.Properties properties)
	{
		this(() -> (FlowerPotBlock) Blocks.FLOWER_POT.delegate.get(), flower, properties);
	}

	public MoolandsFlowerPotBlock(java.util.function.Supplier<? extends Block> flower)
	{
		this(flower, Block.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(0.0F));
	}
}
