package com.legacy.moolands.item.group;

import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class MoolandsItemGroup extends ItemGroup
{
	public static final MoolandsItemGroup ALL = new MoolandsItemGroup("all")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon()
		{
			return new ItemStack(MoolandsBlocks.mooland_grass_block);
		}
	};

	public MoolandsItemGroup(String label)
	{
		super("moolands." + label);
	}
}