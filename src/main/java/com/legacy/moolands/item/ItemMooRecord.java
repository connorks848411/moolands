package com.legacy.moolands.item;

import net.minecraft.item.Item;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ItemMooRecord extends MusicDiscItem
{
	public ItemMooRecord(SoundEvent soundIn, Item.Properties builder)
	{
		super(1, () -> soundIn, builder);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public IFormattableTextComponent getDescription()
	{
		return new TranslationTextComponent("Jon Lachney - Moo");
	}
}