package com.legacy.moolands.client;

import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.structure_gel.events.RenderCloudsEvent;

import net.minecraftforge.eventbus.api.SubscribeEvent;

public class MoolandsClientEvents
{
	@SubscribeEvent
	public void onRenderClouds(RenderCloudsEvent event)
	{
		if (event.getWorld().getDimensionKey() == MoolandsDimensions.moolandsKey())
			event.setCanceled(true);
	}
}
