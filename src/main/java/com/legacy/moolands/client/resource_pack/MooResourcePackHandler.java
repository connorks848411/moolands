package com.legacy.moolands.client.resource_pack;

import com.legacy.moolands.Moolands;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourcePackInfo;
import net.minecraft.util.ResourceLocation;

public class MooResourcePackHandler
{
	/**
	 * Easy registry for resource-packs.
	 */
	public static void init()
	{
		registerResourcePack(Moolands.locate("legacy_pack"), "assets/" + Moolands.MODID + "/legacy_pack/");
	}

	private static void registerResourcePack(ResourceLocation packId, String subFolder)
	{
		Minecraft mc = Minecraft.getInstance();
		mc.getResourcePackList().addPackFinder((consumer, factory) -> consumer.accept(ResourcePackInfo.createResourcePack(packId.toString(), false, () -> new MooResourcePack(subFolder), factory, ResourcePackInfo.Priority.TOP, name -> name)));
	}
}
