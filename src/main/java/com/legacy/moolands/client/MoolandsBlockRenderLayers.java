package com.legacy.moolands.client;

import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;

public class MoolandsBlockRenderLayers
{
	public static void init()
	{
		RenderTypeLookup.setRenderLayer(MoolandsBlocks.mooland_portal, RenderType.getTranslucent());
		renderCutout(MoolandsBlocks.mooland_grass);
		renderCutout(MoolandsBlocks.blue_mushroom);
		renderCutout(MoolandsBlocks.yellow_mushroom);
		renderCutout(MoolandsBlocks.potted_blue_mushroom);
		renderCutout(MoolandsBlocks.potted_yellow_mushroom);
	}

	private static void renderCutout(Block block)
	{
		RenderTypeLookup.setRenderLayer(block, RenderType.getCutout());
	}
}
