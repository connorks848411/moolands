package com.legacy.moolands.client.render;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.client.render.models.BlueCowModel;
import com.legacy.moolands.entities.AwfulCowEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.CowModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CowSaddleLayer extends LayerRenderer<AwfulCowEntity, CowModel<AwfulCowEntity>>
{
	private static final ResourceLocation SADDLE = Moolands.locate("textures/entity/saddle.png");
	private final BlueCowModel<AwfulCowEntity> cowModel = new BlueCowModel<>(0.5F);
	
	/*private static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/pig/pig_saddle.png");
	private final PigModel<AwfulCowEntity> pigModel = new PigModel<>(0.5F);*/

	public CowSaddleLayer(IEntityRenderer<AwfulCowEntity, CowModel<AwfulCowEntity>> p_i50927_1_)
	{
		super(p_i50927_1_);
	}

	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, AwfulCowEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (entitylivingbaseIn.getSaddled())
		{
			this.getEntityModel().copyModelAttributesTo(this.cowModel);
			this.cowModel.setLivingAnimations(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks);
			this.cowModel.setRotationAngles(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			IVertexBuilder ivertexbuilder = bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(SADDLE));
			this.cowModel.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}