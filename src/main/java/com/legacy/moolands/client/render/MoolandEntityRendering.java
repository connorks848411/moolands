package com.legacy.moolands.client.render;

import com.legacy.moolands.registry.MoolandsEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class MoolandEntityRendering
{

	public static void initialization()
	{
		register(MoolandsEntityTypes.AWFUL_COW, AwfulCowRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}

}