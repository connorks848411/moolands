package com.legacy.moolands.client.render.models;

import com.legacy.moolands.entities.AwfulCowEntity;

import net.minecraft.client.renderer.entity.model.CowModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class YellowCowModel<T extends Entity> extends CowModel<T>
{

	public YellowCowModel(float scale)
	{
		super();
		this.headModel = new ModelRenderer(this, 0, 0);
		this.headModel.addBox(-4F, -4F, -6F, 8, 8, 6, 0.0F);
		this.headModel.setRotationPoint(0.0F, 4F, -8F);
		this.headModel.setTextureOffset(22, 0).addBox(-5F, -5F, -4F, 1, 3, 1, 0.0F);
		this.headModel.setTextureOffset(22, 0).addBox(4F, -5F, -4F, 1, 3, 1, 0.0F);

		this.body = new ModelRenderer(this, 18, 4);
		this.body.addBox(-6F, -10F, -7F, 12, 18, 10, scale);
		this.body.setRotationPoint(0.0F, 5F, 2.0F);
		this.body.setTextureOffset(52, 0).addBox(-2F, 2.0F, -8F, 4, 6, 1);

		legFrontRight.mirror = true;
		legBackRight.mirror = true;

		this.swingProgress += 2.0F;
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		AwfulCowEntity cow = (AwfulCowEntity) entityIn;

		if (cow.getCowType() == 1)
		{
			headModel.rotateAngleX = headPitch / 57.29578F + MathHelper.cos(limbSwing * 5.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			headModel.rotateAngleY = netHeadYaw / 57.29578F + MathHelper.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			body.rotateAngleX = 1.570796F + MathHelper.cos(limbSwing * 5.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			legBackRight.rotateAngleX = MathHelper.cos(limbSwing * 2.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			legBackLeft.rotateAngleX = MathHelper.cos(limbSwing * 1.6662F + 9.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			legFrontRight.rotateAngleX = MathHelper.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
			legFrontLeft.rotateAngleX = MathHelper.cos(limbSwing * 1.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + MathHelper.cos(5.807793F);
		}
		else
		{
			this.headModel.rotateAngleX = headPitch / 57.29578F + MathHelper.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.headModel.rotateAngleY = netHeadYaw / 57.29578F + MathHelper.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.body.rotateAngleX = 1.570796F + MathHelper.cos(limbSwing * 1.6662F) * 1.4F * limbSwingAmount;
			this.legBackRight.rotateAngleX = MathHelper.cos(limbSwing * 2.6662F) * 1.4F * limbSwingAmount;
			this.legBackLeft.rotateAngleX = MathHelper.cos(limbSwing * 1.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.legFrontRight.rotateAngleX = MathHelper.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.legFrontLeft.rotateAngleX = MathHelper.cos(limbSwing * 1.6662F) * 1.4F * limbSwingAmount;
		}
	}
}
