package com.legacy.moolands.client.render;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.client.render.models.BlueCowModel;
import com.legacy.moolands.client.render.models.YellowCowModel;
import com.legacy.moolands.entities.AwfulCowEntity;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.model.CowModel;
import net.minecraft.util.ResourceLocation;

public class AwfulCowRenderer extends MobRenderer<AwfulCowEntity, CowModel<AwfulCowEntity>>
{
	private static final ResourceLocation YELLOW_COW = Moolands.locate("textures/entity/yellow_cow.png");
	private static final ResourceLocation BLUE_COW = Moolands.locate("textures/entity/blue_cow.png");
	private static final ResourceLocation JONATHING_COW = Moolands.locate("textures/entity/jonathing.png");

	private static final CowModel<AwfulCowEntity> YELLOW_COW_MODEL = new YellowCowModel<>(0.01F);
	private static final CowModel<AwfulCowEntity> BLUE_COW_MODEL = new BlueCowModel<>(0.01F);

	public AwfulCowRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new CowModel<>(), 0.7F);
		this.addLayer(new CowSaddleLayer(this));
	}

	@Override
	public void render(AwfulCowEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		this.entityModel = entityIn.getCowType() == 1 ? YELLOW_COW_MODEL : BLUE_COW_MODEL;
	}

	@Override
	protected void applyRotations(AwfulCowEntity entityLiving, MatrixStack matrixStackIn, float ageInTicks, float rotationYaw, float partialTicks)
	{
		if (entityLiving.world.getDimensionKey() != MoolandsDimensions.moolandsKey())
		{
			rotationYaw += (float) (Math.cos((double) entityLiving.ticksExisted * 3.25D) * Math.PI * 0.25D);
		}

		super.applyRotations(entityLiving, matrixStackIn, ageInTicks, rotationYaw, partialTicks);
	}

	@Override
	public ResourceLocation getEntityTexture(AwfulCowEntity entity)
	{
		return entity.hasCustomName() && entity.getCustomName().getUnformattedComponentText().equalsIgnoreCase("Jonathing") ? JONATHING_COW : entity.getCowType() == 1 ? YELLOW_COW : BLUE_COW;
	}
}
