package com.legacy.moolands.client;

import com.legacy.moolands.Moolands;

import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder("moolands")
public class SoundsMoolands
{

	public static final SoundEvent COW_AMBIENT = new SoundEvent(Moolands.locate("cow_ambient"));

	public static final SoundEvent COW_HURT = new SoundEvent(Moolands.locate("cow_hurt"));

	public static final SoundEvent COW_DEATH = new SoundEvent(Moolands.locate("cow_death"));

	public static final SoundEvent MOO_DISC = new SoundEvent(Moolands.locate("moo_disc"));

}