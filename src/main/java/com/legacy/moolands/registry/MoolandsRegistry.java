package com.legacy.moolands.registry;

import java.util.Map.Entry;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.client.SoundsMoolands;
import com.legacy.moolands.item.ItemMooRecord;
import com.legacy.moolands.item.group.MoolandsItemGroup;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Rarity;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.village.PointOfInterestType;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = Moolands.MODID, bus = Bus.MOD)
public class MoolandsRegistry
{
	@SubscribeEvent
	public static void onRegisterBlocks(Register<Block> event)
	{
		MoolandsBlocks.init(event);
	}

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "awful_cow_spawn_egg", new SpawnEggItem(MoolandsEntityTypes.AWFUL_COW, 0x33599c, 0xd4cd37, new Item.Properties().group(ItemGroup.MISC)));
		register(event.getRegistry(), "moo_disc", new ItemMooRecord(SoundsMoolands.MOO_DISC, (new Item.Properties()).maxStackSize(1).group(MoolandsItemGroup.ALL).rarity(Rarity.EPIC)));

		for (Entry<Block, ItemGroup> entry : MoolandsBlocks.blockItemMap.entrySet())
		{
			MoolandsRegistry.register(event.getRegistry(), entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), new Item.Properties().group(entry.getValue())));
		}
		MoolandsBlocks.blockItemMap.clear();

		for (Entry<Block, Item.Properties> entry : MoolandsBlocks.blockItemPropertiesMap.entrySet())
		{
			MoolandsRegistry.register(event.getRegistry(), entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), entry.getValue()));
		}
		MoolandsBlocks.blockItemPropertiesMap.clear();
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		MoolandsEntityTypes.init(event);
	}

	@SubscribeEvent
	public static void onRegisterSoundEvents(Register<SoundEvent> event)
	{
		register(event.getRegistry(), "cow_ambient", SoundsMoolands.COW_AMBIENT);
		register(event.getRegistry(), "cow_hurt", SoundsMoolands.COW_HURT);
		register(event.getRegistry(), "cow_death", SoundsMoolands.COW_DEATH);
		register(event.getRegistry(), "moo_disc", SoundsMoolands.MOO_DISC);
	}

	@SubscribeEvent
	public static void registerFeatures(Register<Feature<?>> event)
	{
		MoolandsFeatures.init(event);
	}

	@SubscribeEvent
	public static void onRegisterBiomes(Register<Biome> event)
	{
		MoolandsBiomes.init(event);
	}
	
	@SubscribeEvent
	public static void registerPointsOfInterest(Register<PointOfInterestType> event)
	{
		MoolandsPointsOfInterest.init(event);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		register(registry, Moolands.locate(name), object);
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, ResourceLocation key, T object)
	{
		object.setRegistryName(key);
		registry.register(object);
	}

}