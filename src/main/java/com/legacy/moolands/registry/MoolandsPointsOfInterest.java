package com.legacy.moolands.registry;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.legacy.moolands.Moolands;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.village.PointOfInterestType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.registries.IForgeRegistry;

public class MoolandsPointsOfInterest
{
	public static final PointOfInterestType MOOLANDS_PORTAL = createPOI("moolands_portal", getAllStates(MoolandsBlocks.mooland_portal), 0, 1);

	public static void init(Register<PointOfInterestType> event)
	{
		register(event.getRegistry(), MOOLANDS_PORTAL);
	}

	private static void register(IForgeRegistry<PointOfInterestType> registryIn, PointOfInterestType typeIn)
	{
		MoolandsRegistry.register(registryIn, new ResourceLocation(typeIn.toString()), typeIn);
		registerStates(typeIn);
	}

	// Workaround until Forge can make this better. We could also do this using an
	// access transformer, but reflection takes less time. (This can change later)
	private static void registerStates(PointOfInterestType type)
	{
		try
		{
			ObfuscationReflectionHelper.findMethod(PointOfInterestType.class, "func_221052_a", PointOfInterestType.class).invoke(null, type);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static PointOfInterestType createPOI(String nameIn, Set<BlockState> blockStatesIn, int maxFreeTicketsIn, int validRangeIn)
	{
		return new PointOfInterestType(Moolands.find(nameIn), blockStatesIn, maxFreeTicketsIn, validRangeIn);
	}

	private static Set<BlockState> getAllStates(Block block)
	{
		return ImmutableSet.copyOf(block.getStateContainer().getValidStates());
	}
}
