package com.legacy.moolands.registry;

import java.util.LinkedHashMap;
import java.util.Map;

import com.legacy.moolands.block.MoolandGrassBlock;
import com.legacy.moolands.block.MoolandMushroomBlock;
import com.legacy.moolands.block.MoolandPortalBlock;
import com.legacy.moolands.block.MoolandTallGrassBlock;
import com.legacy.moolands.block.MoolandsFlowerPotBlock;
import com.legacy.moolands.item.group.MoolandsItemGroup;
import com.legacy.structure_gel.blocks.GelPortalBlock;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.HugeMushroomBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.StairsBlock;
import net.minecraft.block.WallBlock;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class MoolandsBlocks
{
	public static Block mooland_portal, mooland_grass, mooland_grass_block, mooland_dirt;

	public static Block moo_rock, moo_rock_slab, moo_rock_stairs, moo_rock_wall;

	public static Block moo_rock_bricks, moo_rock_brick_slab, moo_rock_brick_stairs, moo_rock_brick_wall;

	public static Block blue_mushroom_block, yellow_mushroom_block, blue_mushroom, yellow_mushroom;

	public static Block potted_blue_mushroom, potted_yellow_mushroom;

	public static Map<Block, ItemGroup> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static IForgeRegistry<Block> iBlockRegistry;

	public static void init(Register<Block> event)
	{
		MoolandsBlocks.iBlockRegistry = event.getRegistry();

		mooland_portal = registerBlock("mooland_portal", new MoolandPortalBlock(() -> World.OVERWORLD, MoolandsDimensions::moolandsKey, () -> MoolandsPointsOfInterest.MOOLANDS_PORTAL, () -> (GelPortalBlock) mooland_portal, () -> Blocks.GLOWSTONE.getDefaultState()));

		mooland_grass = register("mooland_grass", new MoolandTallGrassBlock());
		mooland_grass_block = register("mooland_grass_block", new MoolandGrassBlock(Block.Properties.from(Blocks.GRASS_BLOCK)));
		mooland_dirt = register("mooland_dirt", new Block(Block.Properties.from(Blocks.DIRT)));
		moo_rock = register("moo_rock", new Block(Block.Properties.from(Blocks.STONE)));

		moo_rock_slab = register("moo_rock_slab", new SlabBlock(Block.Properties.from(Blocks.COBBLESTONE_SLAB)));
		moo_rock_stairs = register("moo_rock_stairs", new StairsBlock(() -> moo_rock.getDefaultState(), Block.Properties.from(moo_rock)));
		moo_rock_wall = register("moo_rock_wall", new WallBlock(Block.Properties.from(moo_rock)));

		moo_rock_bricks = register("moo_rock_bricks", new Block(Block.Properties.from(Blocks.STONE_BRICKS)));
		moo_rock_brick_slab = register("moo_rock_brick_slab", new SlabBlock(Block.Properties.from(Blocks.STONE_BRICK_SLAB)));
		moo_rock_brick_stairs = register("moo_rock_brick_stairs", new StairsBlock(() -> moo_rock_bricks.getDefaultState(), Block.Properties.from(moo_rock_bricks)));
		moo_rock_brick_wall = register("moo_rock_brick_wall", new WallBlock(Block.Properties.from(moo_rock_bricks)));

		blue_mushroom_block = register("blue_mushroom_block", new HugeMushroomBlock(Block.Properties.from(Blocks.RED_MUSHROOM_BLOCK)));
		yellow_mushroom_block = register("yellow_mushroom_block", new HugeMushroomBlock(Block.Properties.from(Blocks.BROWN_MUSHROOM_BLOCK)), null);
		blue_mushroom = register("blue_mushroom", new MoolandMushroomBlock(Block.Properties.from(Blocks.RED_MUSHROOM)));
		yellow_mushroom = register("yellow_mushroom", new MoolandMushroomBlock(Block.Properties.from(Blocks.BROWN_MUSHROOM)));

		potted_blue_mushroom = registerBlock("potted_blue_mushroom", new MoolandsFlowerPotBlock(() -> blue_mushroom.delegate.get()));
		potted_yellow_mushroom = registerBlock("potted_yellow_mushroom", new MoolandsFlowerPotBlock(() -> yellow_mushroom.delegate.get()));
	}

	public static void setBlockRegistry(IForgeRegistry<Block> iBlockRegistry)
	{
		MoolandsBlocks.iBlockRegistry = iBlockRegistry;
	}

	public static Block register(String name, Block block)
	{
		register(name, block, MoolandsItemGroup.ALL);
		return block;
	}

	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockItemMap.put(block, itemGroup);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String name, Block block)
	{
		if (iBlockRegistry != null)
		{
			MoolandsRegistry.register(iBlockRegistry, name, block);
		}

		return block;
	}
}