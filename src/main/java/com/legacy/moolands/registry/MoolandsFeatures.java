package com.legacy.moolands.registry;

import com.legacy.moolands.world.biome.feature.BigBlueMushroomFeature;
import com.legacy.moolands.world.biome.feature.MoolandLakesFeature;

import net.minecraft.world.biome.BiomeGenerationSettings;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.BigMushroomFeatureConfig;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.BlockStateFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.placement.NoiseDependant;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.event.RegistryEvent.Register;

public class MoolandsFeatures
{
	public static final Feature<BigMushroomFeatureConfig> BLUE_MUSHROOM = new BigBlueMushroomFeature(BigMushroomFeatureConfig.field_236528_a_);
	public static final Feature<BlockStateFeatureConfig> MOOLAND_LAKE = new MoolandLakesFeature(BlockStateFeatureConfig.field_236455_a_);

	public static void init(Register<Feature<?>> event)
	{
		MoolandsRegistry.register(event.getRegistry(), "blue_mushroom", BLUE_MUSHROOM);
		MoolandsRegistry.register(event.getRegistry(), "mooland_lake", MOOLAND_LAKE);
	}

	public static void addNoiseBasedGrass(BiomeGenerationSettings.Builder biomeIn, BlockClusterFeatureConfig config)
	{
		biomeIn.withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(config).withPlacement(Features.Placements.PATCH_PLACEMENT).withPlacement(Placement.field_242900_d.configure(new NoiseDependant(-0.8D, 5, 10))));
	}

	public static void addGrass(BiomeGenerationSettings.Builder biomeIn, BlockClusterFeatureConfig config, int frequency)
	{
		biomeIn.withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(config).withPlacement(Features.Placements.PATCH_PLACEMENT).func_242731_b(frequency));
	}

	public static void addFlowers(BiomeGenerationSettings.Builder biomeIn, BlockClusterFeatureConfig config, int frequency)
	{
		biomeIn.withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.FLOWER.withConfiguration(config).withPlacement(Features.Placements.VEGETATION_PLACEMENT).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).func_242731_b(frequency)); // COUNT_HEIGHTMAP_32
	}
}
