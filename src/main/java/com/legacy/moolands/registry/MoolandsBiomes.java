package com.legacy.moolands.registry;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.world.gen.StrangeChunkGenerator;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityClassification;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeAmbience;
import net.minecraft.world.biome.BiomeGenerationSettings;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.biome.MoodSoundAmbience;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.blockstateprovider.WeightedBlockStateProvider;
import net.minecraft.world.gen.feature.BigMushroomFeatureConfig;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.BlockStateFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.NoiseDependant;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class MoolandsBiomes
{
	public static final Biome AWKWARD_HEIGHTS = Builders.createAwkwardHeightsBiome();
	public static final RegistryKey<Biome> AWKWARD_HEIGHTS_KEY = RegistryKey.getOrCreateKey(Registry.BIOME_KEY, Moolands.locate("awkward_heights"));

	public static void init(Register<Biome> event)
	{
		register(event.getRegistry(), "awkward_heights", AWKWARD_HEIGHTS);
		MoolandsDimensions.initNoiseSettings();

		Registry.register(Registry.CHUNK_GENERATOR_CODEC, "strange_noise", StrangeChunkGenerator.providerCodec);

	}

	public static void register(IForgeRegistry<Biome> registry, String key, Biome biome)
	{
		MoolandsRegistry.register(registry, key, biome);
	}

	public static class Builders
	{
		public static final BlockClusterFeatureConfig TALL_GRASS = (new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(MoolandsBlocks.mooland_grass.getDefaultState()), new SimpleBlockPlacer())).tries(32).build();
		public static final BlockClusterFeatureConfig SMALL_MUSHROOM_CONFIG = (new BlockClusterFeatureConfig.Builder((new WeightedBlockStateProvider()).addWeightedBlockstate(MoolandsBlocks.blue_mushroom.getDefaultState(), 1).addWeightedBlockstate(MoolandsBlocks.yellow_mushroom.getDefaultState(), 1), new SimpleBlockPlacer())).tries(64).build();

		public static Biome createAwkwardHeightsBiome()
		{
			MobSpawnInfo.Builder spawns = new MobSpawnInfo.Builder();

			spawns.withSpawner(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(MoolandsEntityTypes.AWFUL_COW, 70, 3, 5));

			BiomeGenerationSettings.Builder builder = (new BiomeGenerationSettings.Builder()).withSurfaceBuilder(MoolandsBiomes.SurfaceBuilders.DREAM_GRASS_SURFACE_BUILDER);

			builder.withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, MoolandsFeatures.BLUE_MUSHROOM.withConfiguration(new BigMushroomFeatureConfig(new SimpleBlockStateProvider(MoolandsBlocks.blue_mushroom_block.getDefaultState()), new SimpleBlockStateProvider(Blocks.MUSHROOM_STEM.getDefaultState()), 2)).func_242733_d(130).func_242728_a().func_242731_b(40));
			builder.withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, MoolandsFeatures.MOOLAND_LAKE.withConfiguration(new BlockStateFeatureConfig(Blocks.WATER.getDefaultState())).withPlacement(Placement.WATER_LAKE.configure(new ChanceConfig(4))));

			builder.withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(TALL_GRASS).withPlacement(Features.Placements.PATCH_PLACEMENT).withPlacement(Placement.field_242900_d.configure(new NoiseDependant(-0.8D, 5, 10))));
			MoolandsFeatures.addFlowers(builder, SMALL_MUSHROOM_CONFIG, 10);
			//2b80ff
			return (new Biome.Builder()).precipitation(Biome.RainType.NONE).category(Biome.Category.NONE).depth(0.1F).scale(0.2F).temperature(0.5F).downfall(0.4F).setEffects((new BiomeAmbience.Builder()).withGrassColor(0xff744a).withFoliageColor(0xff744a).withSkyColor(0x2b80ff).setWaterColor(0xffffff).setWaterFogColor(0xffffff).setFogColor(12638463).setMoodSound(MoodSoundAmbience.DEFAULT_CAVE).build()).withMobSpawnSettings(spawns.copy()).withGenerationSettings(builder.build()).build();
		}
	}

	public static class SurfaceBuilders
	{
		public static final SurfaceBuilderConfig DREAM_GRASS_DIRT_GRAVEL_CONFIG = new SurfaceBuilderConfig(MoolandsBlocks.mooland_grass_block.getDefaultState(), MoolandsBlocks.mooland_dirt.getDefaultState(), Blocks.GRAVEL.getDefaultState());

		public static final ConfiguredSurfaceBuilder<SurfaceBuilderConfig> DREAM_GRASS_SURFACE_BUILDER = SurfaceBuilder.DEFAULT.func_242929_a(DREAM_GRASS_DIRT_GRAVEL_CONFIG);
	}
}
