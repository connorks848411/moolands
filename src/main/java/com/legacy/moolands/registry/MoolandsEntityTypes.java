package com.legacy.moolands.registry;

import java.util.Random;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.entities.AwfulCowEntity;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(Moolands.MODID)
public class MoolandsEntityTypes
{
	public static final EntityType<AwfulCowEntity> AWFUL_COW = buildEntity("awful_cow", EntityType.Builder.create(AwfulCowEntity::new, EntityClassification.CREATURE).size(0.9F, 1.4F));

	public static void init(Register<EntityType<?>> event)
	{
		MoolandsRegistry.register(event.getRegistry(), "awful_cow", AWFUL_COW);

		registerSpawnConditions();
		GlobalEntityTypeAttributes.put(AWFUL_COW, AwfulCowEntity.registerAttributes().create());
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(Moolands.find(key));
	}

	private static boolean animalSpawnConditions(EntityType<? extends AnimalEntity> type, IWorld world, SpawnReason reason, BlockPos pos, Random rand)
	{
		Block block = world.getBlockState(pos.down()).getBlock();
		return block == MoolandsBlocks.mooland_grass_block && world.getLightSubtracted(pos, 0) > 8;
	}

	private static void registerSpawnConditions()
	{
		EntitySpawnPlacementRegistry.register(MoolandsEntityTypes.AWFUL_COW, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MoolandsEntityTypes::animalSpawnConditions);
	}
}
