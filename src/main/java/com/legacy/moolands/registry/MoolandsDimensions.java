package com.legacy.moolands.registry;

import java.lang.reflect.Constructor;
import java.util.OptionalLong;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.moolands.Moolands;
import com.legacy.moolands.world.gen.StrangeChunkGenerator;
import com.legacy.structure_gel.events.RegisterDimensionEvent;
import com.legacy.structure_gel.registrars.DimensionRegistrar;
import com.legacy.structure_gel.util.RegistryHelper;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.Dimension;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.FuzzedBiomeMagnifier;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.DimensionSettings;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.gen.settings.NoiseSettings;
import net.minecraft.world.gen.settings.ScalingSettings;
import net.minecraft.world.gen.settings.SlideSettings;

public class MoolandsDimensions
{
	public static final ResourceLocation MOOLANDS_ID = Moolands.locate("moolands");
	public static final RegistryKey<World> MOOLANDS = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, MOOLANDS_ID);

	public static final RegistryKey<DimensionSettings> MOOLANDS_NOISE_SETTINGS = RegistryKey.getOrCreateKey(Registry.NOISE_SETTINGS_KEY, MOOLANDS_ID);

	public static final RegistryKey<DimensionType> MOOLANDS_TYPE = RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, MOOLANDS_ID);

	public static final RegistryKey<Dimension> MOOLANDS_DIM = RegistryKey.getOrCreateKey(Registry.DIMENSION_KEY, MOOLANDS_ID);

	public static void init(RegisterDimensionEvent eventIn)
	{
		Function<RegistryKey<DimensionSettings>, DimensionSettings> mooSettings = (noiseSettings) -> WorldGenRegistries.NOISE_SETTINGS.getOrThrow(MOOLANDS_NOISE_SETTINGS);
		BiFunction<RegisterDimensionEvent, DimensionSettings, ChunkGenerator> mooGenerator = (event, s) -> createStrangeChunkGenerator(event.getBiomeRegistry(), event.getDimensionSettingsRegistry(), event.getSeed());
		Supplier<DimensionType> mooDimensionType = () -> createDimSettings(OptionalLong.empty(), false, false);

		RegistryHelper.handleRegistrar(new DimensionRegistrar(eventIn, MOOLANDS_ID, mooDimensionType, mooSettings, mooGenerator));
	}

	public static void initNoiseSettings()
	{
		registerNoiseSettings(MOOLANDS_NOISE_SETTINGS, createNoiseSettings(new DimensionStructuresSettings(false), false, MoolandsBlocks.moo_rock.getDefaultState(), Blocks.AIR.getDefaultState(), MOOLANDS_NOISE_SETTINGS.getLocation()));
	}

	public static DimensionSettings createNoiseSettings(DimensionStructuresSettings structureSettingsIn, boolean flag1, BlockState fillerBlockIn, BlockState fluidBlockIn, ResourceLocation settingsLocationIn)
	{
		try
		{
			Constructor<DimensionSettings> constructor = DimensionSettings.class.getDeclaredConstructor(DimensionStructuresSettings.class, NoiseSettings.class, BlockState.class, BlockState.class, int.class, int.class, int.class, boolean.class);
			constructor.setAccessible(true);
			return constructor.newInstance(structureSettingsIn, new NoiseSettings(256, new ScalingSettings(0.9999999814507745D, 0.9999999814507745D, 80.0D, 160.0D), new SlideSettings(-10, 3, 0), new SlideSettings(-30, 0, 0), 1, 2, 1.0D, -0.46875D, true, true, false, flag1), fillerBlockIn, fluidBlockIn, -10, 0, 63, false);
		}
		catch (Exception e)
		{
			Moolands.LOGGER.error("Failed to create dimension settings. This issue should be reported!");
			e.printStackTrace();
		}

		return null;
	}

	private static DimensionSettings registerNoiseSettings(RegistryKey<DimensionSettings> settingsKeyIn, DimensionSettings dimSettingsIn)
	{
		WorldGenRegistries.register(WorldGenRegistries.NOISE_SETTINGS, settingsKeyIn.getLocation(), dimSettingsIn);
		return dimSettingsIn;
	}

	private static ChunkGenerator createStrangeChunkGenerator(Registry<Biome> biomeRegistry, Registry<DimensionSettings> dimSettingsRegistry, long seed)
	{
		return new StrangeChunkGenerator(new SingleBiomeProvider(biomeRegistry.getOrThrow(MoolandsBiomes.AWKWARD_HEIGHTS_KEY)), seed, () -> dimSettingsRegistry.getOrThrow(MoolandsDimensions.MOOLANDS_NOISE_SETTINGS));
	}

	public static RegistryKey<World> moolandsKey()
	{
		return MOOLANDS;
	}

	private static DimensionType createDimSettings(OptionalLong time, boolean ultrawarm, boolean piglinSafe)
	{
		return new DimensionType(time, true, false, ultrawarm, true, 1, false, piglinSafe, true, false, false, 256, FuzzedBiomeMagnifier.INSTANCE, BlockTags.INFINIBURN_OVERWORLD.getName(), DimensionType.OVERWORLD_ID, 0.0F)
		{
		};
	}
}
