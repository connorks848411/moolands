package com.legacy.moolands.entities;

import com.legacy.moolands.client.SoundsMoolands;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.moolands.registry.MoolandsEntityTypes;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.BreedGoal;
import net.minecraft.entity.ai.goal.FollowParentGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class AwfulCowEntity extends SaddleMountEntity
{
	public static final DataParameter<Integer> TYPE = EntityDataManager.<Integer>createKey(AwfulCowEntity.class, DataSerializers.VARINT);

	public AwfulCowEntity(EntityType<? extends AwfulCowEntity> type, World worldIn)
	{
		super(type, worldIn);

		this.setCowType(this.rand.nextInt(2));
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 2.0D));
		this.goalSelector.addGoal(2, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(4, new FollowParentGoal(this, 1.25D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 6.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
	}

	public static AttributeModifierMap.MutableAttribute registerAttributes()
	{
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20.0D).createMutableAttribute(Attributes.MOVEMENT_SPEED, (double) 0.2F);
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(TYPE, 0);
	}

	@Override
	public boolean onLivingFall(float distance, float damageMultiplier)
	{
		return false;
	}

	public void setCowType(int type)
	{
		this.dataManager.set(TYPE, type);
	}

	public int getCowType()
	{
		return this.dataManager.get(TYPE);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (!this.getPassengers().isEmpty())
		{
			return;
		}

		if (this.getCowType() == 1)
		{
			if (this.onGround)
			{
				if (this.moveForward != 0.0F)
				{
					this.jump();
				}
			}
			else if (this.moveForward != 0.0F)
			{
				if ((!this.world.isAirBlock(this.getPosition().down()) || !this.world.isAirBlock(this.getPosition().down(2))) && this.world.isAirBlock(this.getPosition().up(2)) && this.world.isAirBlock(this.getPosition().up()))
				{
					this.setMotion(this.getMotion().getX(), 0.20000000000000001D, this.getMotion().getZ());
				}
			}

			if (this.getMotion().getY() < -0.10000000000000001D)
			{
				this.setMotion(this.getMotion().getX(), -0.10000000000000001D, this.getMotion().getZ());
			}

			// wee

			if (this.world.isRemote && !this.onGround)
			{
				// this.yellowCowParticles();
			}
		}

		if (this.world.isRemote && this.getMotion().getX() == 0F && this.getMotion().getZ() == 0F && this.onGround)
		{
			for (int i = 0; i < 2; ++i)
			{
				// this.world.spawnParticle(EnumParticleTypes.WATER_SPLASH, this.posX +
				// (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY +
				// this.rand.nextDouble() * (double)this.height, this.posZ +
				// (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D);
			}
		}
	}

	/*@SideOnly(Side.CLIENT)
	public void yellowCowParticles()
	{
	    FMLClientHandler.instance().getClient().effectRenderer.addEffect(new EntityGoldenFX(world, this.posX + (this.rand.nextDouble() - 0.75D), this.posY + (this.rand.nextDouble() - 0.75D), this.posZ + (this.rand.nextDouble() - 0.75D), motionX, motionY, motionZ));
	}*/

	@Override
	protected void dropFewItems(boolean par1, int par2)
	{
		if (this.getSaddled())
		{
			this.entityDropItem(Items.SADDLE, 1);
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return SoundsMoolands.COW_AMBIENT;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return SoundsMoolands.COW_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundsMoolands.COW_DEATH;
	}

	@Override
	protected float getSoundVolume()
	{
		return 0.3F;
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("CowType", this.getCowType());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setCowType(compound.getInt("CowType"));
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld world, AgeableEntity ageable)
	{
		return MoolandsEntityTypes.AWFUL_COW.create(this.world);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return this.isChild() ? sizeIn.height * 0.95F : 1.3F;
	}

	@Override
	public float getMountedMoveSpeed()
	{
		return this.world.getDimensionKey() == MoolandsDimensions.moolandsKey() ? 0.7F : 0.5F;
	}

	@Override
	protected double getMountJumpStrength()
	{
		return this.world.getDimensionKey() == MoolandsDimensions.moolandsKey() ? 1.0D : 0.6D;
	}
}
