package com.legacy.moolands.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public abstract class SaddleMountEntity extends MountableEntity
{

	public static final DataParameter<Boolean> SADDLED = EntityDataManager.<Boolean>createKey(MountableEntity.class, DataSerializers.BOOLEAN);

	public SaddleMountEntity(EntityType<? extends SaddleMountEntity> type, World world)
	{
		super(type, world);
	}

	@Override
	public boolean attackEntityFrom(DamageSource damagesource, float i)
	{
		if ((damagesource.getImmediateSource() instanceof PlayerEntity) && (!this.getPassengers().isEmpty() && this.getPassengers().get(0) == damagesource.getImmediateSource()))
		{
			return false;
		}

		return super.attackEntityFrom(damagesource, i);
	}

	@Override
	public boolean canBeSteered()
	{
		return true;
	}

	@Override
	protected boolean canTriggerWalking()
	{
		return this.onGround;
	}

	protected void dropFewItems(boolean var1, int var2)
	{
		this.dropSaddle();
	}

	protected void dropSaddle()
	{
		if (this.getSaddled())
		{
			this.entityDropItem(Items.SADDLE, 1);
		}
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(SADDLED, false);
	}

	public boolean getSaddled()
	{
		return this.dataManager.get(SADDLED);
	}

	public boolean canSaddle()
	{
		return true;
	}

	@Override
	public ActionResultType func_230254_b_(PlayerEntity PlayerEntity, Hand hand)
	{
		if (!this.canSaddle())
		{
			return super.func_230254_b_(PlayerEntity, hand);
		}

		if (!this.getSaddled())
		{
			ItemStack currentStack = PlayerEntity.getHeldItem(hand);

			if (currentStack.getItem() == Items.SADDLE && !this.isChild())
			{
				if (!PlayerEntity.isCreative())
				{
					currentStack.shrink(1);
				}

				PlayerEntity.world.playSound(PlayerEntity, PlayerEntity.getPosition(), SoundEvents.ENTITY_PIG_SADDLE, SoundCategory.AMBIENT, 1.0F, 1.0F);

				this.setSaddled(true);

				return ActionResultType.SUCCESS;
			}
		}
		else
		{
			if (this.getPassengers().isEmpty())
			{
				if (!PlayerEntity.world.isRemote)
				{
					PlayerEntity.startRiding(this);
					PlayerEntity.prevRotationYaw = PlayerEntity.rotationYaw = this.rotationYaw;
				}

				return ActionResultType.SUCCESS;
			}
		}

		return super.func_230254_b_(PlayerEntity, hand);
	}

	@Override
	public boolean isEntityInsideOpaqueBlock()
	{
		if (!this.getPassengers().isEmpty())
		{
			return false;
		}

		return super.isEntityInsideOpaqueBlock();
	}

	@Override
	public void read(CompoundNBT nbttagcompound)
	{
		super.read(nbttagcompound);

		this.setSaddled(nbttagcompound.getBoolean("getSaddled"));
	}

	public void setSaddled(boolean saddled)
	{
		this.dataManager.set(SADDLED, saddled);
	}

	@Override
	public boolean shouldRiderFaceForward(PlayerEntity player)
	{
		return false;
	}

	@Override
	public void writeAdditional(CompoundNBT nbttagcompound)
	{
		super.writeAdditional(nbttagcompound);

		nbttagcompound.putBoolean("getSaddled", this.getSaddled());
	}

}