package com.legacy.moolands.entities;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public abstract class MountableEntity extends AnimalEntity
{

	public static final DataParameter<Boolean> RIDER_SNEAKING = EntityDataManager.<Boolean>createKey(MountableEntity.class, DataSerializers.BOOLEAN);

	protected float jumpPower;

	protected boolean mountJumping;

	protected boolean canJumpMidAir = false;

	public MountableEntity(EntityType<? extends MountableEntity> type, World world)
	{
		super(type, world);
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(RIDER_SNEAKING, false);
	}

	@Override
	public boolean canRiderInteract()
	{
		return true;
	}

	@Override
	public boolean canBeRiddenInWater(Entity rider)
	{
		return false;
	}

	public boolean isRiderSneaking()
	{
		return this.dataManager.get(RIDER_SNEAKING);
	}

	public void setRiderSneaking(boolean riderSneaking)
	{
		this.dataManager.set(RIDER_SNEAKING, riderSneaking);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void travel(Vector3d vec3d)
	{
		double moveStrafing = vec3d.getX();
		double moveVertical = vec3d.getY();
		double moveForward = vec3d.getZ();
		Entity entity = this.getPassengers().isEmpty() ? null : this.getPassengers().get(0);

		if (entity instanceof PlayerEntity)
		{
			PlayerEntity player = (PlayerEntity) entity;

			boolean hasJumped = ObfuscationReflectionHelper.getPrivateValue(LivingEntity.class, player, "field_70703_bu");

			this.prevRotationYaw = this.rotationYaw = player.rotationYaw;
			this.prevRotationPitch = this.rotationPitch = player.rotationPitch;

			this.rotationYawHead = player.rotationYawHead;

			moveStrafing = player.moveStrafing;
			moveVertical = player.moveVertical;
			moveForward = player.moveForward;

			if (moveForward <= 0.0F)
			{
				moveForward *= 0.25F;
			}

			if (hasJumped)
			{
				onMountedJump(moveStrafing, moveForward);
			}

			if (this.jumpPower > 0.0F && !this.isMountJumping() && (this.onGround || this.canJumpMidAir))
			{
				this.setMotion(this.getMotion().x, this.getMountJumpStrength() * (double) this.jumpPower, this.getMotion().z);

				if (this.isPotionActive(Effects.JUMP_BOOST))
				{
					this.setMotion(this.getMotion().add(0, (this.getActivePotionEffect(Effects.JUMP_BOOST).getAmplifier() + 1) * 0.1F, 0));
				}

				this.setMountJumping(true);
				this.isAirBorne = true;

				this.jumpPower = 0.0F;

				if (!this.world.isRemote)
				{
					this.move(MoverType.SELF, vec3d);
				}
			}

			this.setMotion(this.getMotion().mul(0.35F, 1, 0.35F));

			this.stepHeight = 1.0F;

			if (!this.world.isRemote)
			{
				this.jumpMovementFactor = this.getAIMoveSpeed() * 0.6F;
				super.travel(new Vector3d(moveStrafing, moveVertical, moveForward));
			}

			if (this.onGround)
			{
				this.jumpPower = 0.0F;
				this.setMountJumping(false);
			}

			this.prevLimbSwingAmount = this.limbSwingAmount;
			double d0 = this.getPosX() - this.prevPosX;
			double d1 = this.getPosZ() - this.prevPosZ;
			float f4 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;

			if (f4 > 1.0F)
			{
				f4 = 1.0F;
			}

			this.limbSwingAmount += (f4 - this.limbSwingAmount) * 0.4F;
			this.limbSwing += this.limbSwingAmount;
		}
		else
		{
			this.stepHeight = 0.5F;
			this.jumpMovementFactor = 0.02F;
			super.travel(vec3d);
		}
	}

	@Override
	public float getAIMoveSpeed()
	{
		return this.getMountedMoveSpeed();
	}

	public float getMountedMoveSpeed()
	{
		return 0.7F;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	protected double getMountJumpStrength()
	{
		return 1.0D;
	}

	protected void setMountJumping(boolean mountJumping)
	{
		this.mountJumping = mountJumping;
	}

	protected boolean isMountJumping()
	{
		return this.mountJumping;
	}

	public void onMountedJump(double par1, double par2)
	{
		this.jumpPower = 1F;
	}

}