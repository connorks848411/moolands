package com.legacy.moolands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.moolands.client.MoolandsBlockRenderLayers;
import com.legacy.moolands.client.MoolandsClientEvents;
import com.legacy.moolands.client.render.MoolandEntityRendering;
import com.legacy.moolands.client.resource_pack.MooResourcePackHandler;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.structure_gel.events.RegisterDimensionEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(Moolands.MODID)
public class Moolands
{
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String NAME = "Moolands";
	public static final String MODID = "moolands";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(Moolands.MODID, name);
	}

	public static String find(String name)
	{
		return Moolands.MODID + ":" + name;
	}

	public Moolands()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientInit));

		MinecraftForge.EVENT_BUS.addListener(this::onRegisterDimensions);
	}

	private void init(final FMLCommonSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new MoolandsEvents());
	}

	public void clientInit(FMLClientSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new MoolandsClientEvents());

		MoolandEntityRendering.initialization();
		MooResourcePackHandler.init();
		MoolandsBlockRenderLayers.init();
	}

	public void onRegisterDimensions(RegisterDimensionEvent event)
	{
		MoolandsDimensions.init(event);
	}
}
