package com.legacy.moolands;

import com.google.common.collect.ImmutableList;
import com.legacy.moolands.block.MoolandPortalBlock;
import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.structure_gel.blocks.GelPortalBlock;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class MoolandsEvents
{
	@SubscribeEvent
	public void onMakePortal(RightClickBlock event)
	{
		if (event.getItemStack() != null && event.getItemStack().getItem() == Items.MILK_BUCKET)
		{
			if (GelPortalBlock.fillPortal(event.getWorld(), event.getPos().offset(event.getFace()), (MoolandPortalBlock) MoolandsBlocks.mooland_portal, ImmutableList.of(Blocks.AIR)))
			{
				event.setCanceled(true);
				event.getPlayer().playSound(SoundEvents.ENTITY_COW_MILK, 1.0F, 1.0F);

				if (event.getPlayer().getActiveHand() != null)
					event.getPlayer().swingArm(event.getPlayer().getActiveHand());

				if (!event.getPlayer().isCreative())
					event.getPlayer().inventory.setInventorySlotContents(event.getPlayer().inventory.currentItem, new ItemStack(Items.BUCKET));
			}
		}
	}

}